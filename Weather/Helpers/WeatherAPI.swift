import Foundation

class WeatherAPI
{
    // MARK: - Configuration
    static let weatherAPIKey = "f8969ab55000c8f5e0d41ec5d580afe6"
    static let weatherAPIAddress = "https://api.openweathermap.org/data/2.5"
    static let units = "metric"
    
    // MARK: Endpoints addresses
    static let cityDataEndpoint = "\(WeatherAPI.weatherAPIAddress)/weather?appid=\(WeatherAPI.weatherAPIKey)&units=\(units)";
    static let ForecastEndpoint = "\(WeatherAPI.weatherAPIAddress)/forecast?appid=\(WeatherAPI.weatherAPIKey)&units=\(units)";
    static let LongForecastEndpoint = "\(WeatherAPI.weatherAPIAddress)/forecast/daily?appid=\(WeatherAPI.weatherAPIKey)&units\(units)";
    
    static func getCityDataURL(cityID id:Int) -> String {
        return "\(cityDataEndpoint)&id=\(id)"

    }

    static func getCityDataURL(cityName name:String) -> String {
        return "\(cityDataEndpoint)&q=\(name)"
    }
    
    static func getForecastURL(cityID id:Int) -> String {
        return "\(ForecastEndpoint)&id=\(id)"
        
    }
    
    static func getForecastURL(cityName name:String) -> String {
        return "\(ForecastEndpoint)&q=\(name)"
    }

    static func getLongForecastURL(cityID id:Int) -> String {
        return "\(LongForecastEndpoint)&id=\(id)"
    }
    
    static func getLongForecastURL(cityName name:String) -> String {
        return "\(LongForecastEndpoint)&q=\(name)"
    }
    
    // MARK: - Functionality
    
    
}
