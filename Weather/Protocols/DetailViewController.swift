//
//  DetailViewController.swift
//  Neme WEATHER
//
//  Created by Georgi Nikolaev on 2.08.18 г..
//  Copyright © 2018 г. Georgi Nikolaev. All rights reserved.
//

import Foundation

public protocol DetailViewController:RefreshableViewController {
    associatedtype detailType
    func show(detail: detailType)
}
