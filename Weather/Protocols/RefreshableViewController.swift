//
//  RefreshableViewController.swift
//  Neme WEATHER
//
//  Created by Georgi Nikolaev on 14.08.18 г..
//  Copyright © 2018 г. Georgi Nikolaev. All rights reserved.
//

import Foundation

public protocol RefreshableViewController {
    var refreshListener: RefreshListenerViewControllerProtocol? {get set}
}
