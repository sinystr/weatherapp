//
//  DetailViewControllerProtocol.swift
//  Neme WEATHER
//
//  Created by Georgi Nikolaev on 9.08.18 г..
//  Copyright © 2018 г. Georgi Nikolaev. All rights reserved.
//

import Foundation

public protocol RefreshListenerViewControllerProtocol : NSObjectProtocol {
    func refresh(completed: () -> Void)
}
