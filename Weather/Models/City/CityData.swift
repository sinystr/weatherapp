import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class CityData : Object, Mappable {    
    @objc dynamic var coord: CityDataCoord?
    var weather = List<CityDataWeather>()
    
    @objc dynamic var base = ""
    @objc dynamic var main: CityDataMain?
    @objc dynamic var visibility = 0
    @objc dynamic var wind: CityDataWind?
    @objc dynamic var clouds: CityDataClouds?
    @objc dynamic var dt = 0
    @objc dynamic var sys: CityDataSys?
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var cod = 0
    @objc dynamic var favorite = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        coord       <- map["coord"]
        weather <- (map["weather"], ListTransform<CityDataWeather>())
        base        <- map["base"]
        main        <- map["main"]
        visibility  <- map["visibility"]
        wind        <- map["wind"]
        clouds      <- map["clouds"]
        dt          <- map["dt"]
        sys         <- map["sys"]
        id          <- map["id"]
        name        <- map["name"]
        cod         <- map["cod"]
    }
}
