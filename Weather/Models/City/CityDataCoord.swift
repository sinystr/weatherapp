import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class CityDataCoord : Object, Mappable {
    @objc dynamic var lon = 0.0
    @objc dynamic var lat = 0.0
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        lon        <- map["lon"]
        lat        <- map["lat"]
    }
}

