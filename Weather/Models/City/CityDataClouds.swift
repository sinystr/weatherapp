import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class CityDataClouds : Object, Mappable {
    @objc dynamic var all = 0
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        all    <- map["all"]
    }
}

