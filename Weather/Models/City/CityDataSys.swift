import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class CityDataSys : Object, Mappable {
    @objc dynamic var type = 0
    @objc dynamic var id = 0
    @objc dynamic var message = 0.0
    @objc dynamic var country = ""
    @objc dynamic var sunrise = 0
    @objc dynamic var sunset = 0
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        type           <- map["type"]
        id             <- map["id"]
        message        <- map["message"]
        country        <- map["country"]
        sunrise        <- map["sunrise"]
        sunset         <- map["sunset"]
    }
}
