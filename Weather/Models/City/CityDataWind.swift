import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class CityDataWind : Object, Mappable {
    @objc dynamic var speed = 0.0
    @objc dynamic var deg = 0
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        speed    <- map["speed"]
        deg      <- map["deg"]
    }
}

