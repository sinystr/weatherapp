import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class CityDataMain : Object, Mappable {
    @objc dynamic var temp = 0.0
    @objc dynamic var pressure = 0
    @objc dynamic var humidity = 0
    @objc dynamic var temp_min = 0.0
    @objc dynamic var temp_max = 0.0
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        temp       <- map["temp"]
        pressure   <- map["pressure"]
        humidity   <- map["humidity"]
        temp_min   <- map["temp_min"]
        temp_max   <- map["temp_max"]
    }
}

