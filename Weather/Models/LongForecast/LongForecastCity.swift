import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class LongForecastCity : Object, Mappable {
    @objc dynamic var geoname_id = 0
    @objc dynamic var name = ""
    @objc dynamic var lat = 0.0
    @objc dynamic var lon = 0.0
    @objc dynamic var country = ""
    @objc dynamic var iso2 = ""
    @objc dynamic var type = ""
    @objc dynamic var population = 0
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        geoname_id  <- map["geoname_id"]
        name         <- map["name"]
        lat          <- map["lat"]
        lon          <- map["lon"]
        country      <- map["country"]
        iso2          <- map["iso2"]
        type          <- map["type"]
        population   <- map["population"]
    }
}

