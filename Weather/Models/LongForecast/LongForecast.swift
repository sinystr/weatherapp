import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class LongForecast : Object, Mappable {
    @objc dynamic var cityID = 0
    @objc dynamic var cod = ""
    @objc dynamic var message = 0
    @objc dynamic var city: LongForecastCity?
    @objc dynamic var cnt = 0
    var list = [LongForecastList]()
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
        return "cityID"
    }

    func mapping(map: Map) {
        cod         <- map["cod"]
        message     <- map["message"]
        cnt         <- map["cnt"]
        list        <- map["list"]
        city        <- map["city"]
        
        if let c = city {
            cityID = c.geoname_id
        }
    }
}

