import Foundation
import ObjectMapper
import AlamofireObjectMapper
import RealmSwift

class LongForecastList : Object, Mappable {
    @objc dynamic var dt = 0
    @objc dynamic var temp: LongForecastTemp?
    @objc dynamic var pressure = 0.0
    @objc dynamic var humidity = 0
    var weather = List<Weather>()
    @objc dynamic var speed = 0.0
    @objc dynamic var deg = 0
    @objc dynamic var clouds = 0
    @objc dynamic var snow = 0.0

    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        dt              <- map["dt"]
        temp            <- map["temp"]
        pressure        <- map["pressure"]
        humidity        <- map["humidity"]
        weather         <- (map["weather"], ListTransform<Weather>())
        speed           <- map["speed"]
        deg             <- map["deg"]
        clouds          <- map["clouds"]
        snow            <- map["snow"]
    }
}

