import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class LongForecastTemp : Object, Mappable {
    @objc dynamic var day = 0.0
    @objc dynamic var min = 0.0
    @objc dynamic var max = 0.0
    @objc dynamic var night = 0.0
    @objc dynamic var eve = 0.0
    @objc dynamic var morn = 0.0
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        day           <- map["day"]
        min           <- map["min"]
        max           <- map["max"]
        night         <- map["night"]
        eve           <- map["eve"]
        morn          <- map["morn"]
    }
}

