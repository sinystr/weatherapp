import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class ForecastListItem : Object, Mappable {
    @objc dynamic var dt = 0
    @objc dynamic var main: ForecastMain?
    var weather = [ForecastWeather]()
    @objc dynamic var clouds: ForecastClouds?
    @objc dynamic var wind: ForecastWind?
    @objc dynamic var snow: ForecastSnow?
    @objc dynamic var sys: ForecastSys?
    @objc dynamic var dt_txt = ""
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        dt        <- map["dt"]
        main      <- map["main"]
        weather   <- map["weather"]
        clouds    <- map["clouds"]
        wind      <- map["wind"]
        snow      <- map["snow"]
        sys       <- map["sys"]
        dt_txt    <- map["dt_txt"]
    }
}

