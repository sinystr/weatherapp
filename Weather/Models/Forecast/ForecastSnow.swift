import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class ForecastSnow : Object, Mappable {
    @objc dynamic var threeHours = 0.0
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        threeHours   <- map["threeHours"]
    }
}
