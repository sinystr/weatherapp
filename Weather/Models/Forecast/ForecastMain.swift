import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class ForecastMain : Object, Mappable {
    @objc dynamic var temp = 0.0
    @objc dynamic var temp_min = 0.0
    @objc dynamic var temp_max = 0.0
    @objc dynamic var pressure = 0
    @objc dynamic var sea_level = 0.0
    @objc dynamic var grnd_level = 0
    @objc dynamic var humidity = 0
    @objc dynamic var temp_kf = 0
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        temp           <- map["temp"]
        temp_min       <- map["temp_min"]
        temp_max       <- map["temp_max"]
        pressure       <- map["pressure"]
        sea_level      <- map["sea_level"]
        grnd_level     <- map["grnd_level"]
        humidity       <- map["humidity"]
        temp_kf        <- map["temp_kf"]
    }
}

