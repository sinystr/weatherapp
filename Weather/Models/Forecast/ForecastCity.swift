import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class ForecastCity : Object, Mappable {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var coord: ForecastCoord?
    @objc dynamic var country = ""
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id      <- map["id"]
        name    <- map["name"]
        coord   <- map["coord"]
        country <- map["country"]
    }
}
