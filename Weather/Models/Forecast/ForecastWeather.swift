import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class ForecastWeather : Object, Mappable {
    @objc dynamic var id = 0
    @objc dynamic var main = ""
    @objc dynamic var descr = ""
    @objc dynamic var icon = ""
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id          <- map["id"]
        main        <- map["main"]
        descr       <- map["description"]
        icon        <- map["icon"]
    }
}

