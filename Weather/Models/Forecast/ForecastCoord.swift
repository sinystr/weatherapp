import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class ForecastCoord : Object, Mappable {
    @objc dynamic var lat = 0.0
    @objc dynamic var lon = 0.0

    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        lat           <- map["lat"]
        lon           <- map["lon"]
    }
}
