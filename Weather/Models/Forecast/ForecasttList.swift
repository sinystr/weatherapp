import Foundation
import RealmSwift

class ForecastList : Object {
    @objc dynamic var forecastListItems: ForecastListItem?
}
