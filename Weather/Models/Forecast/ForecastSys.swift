import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class ForecastSys : Object, Mappable {
    @objc dynamic var pod = ""
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        pod           <- map["pod"]
    }
}
