import Foundation
import ObjectMapper
import AlamofireObjectMapper
import RealmSwift

class Forecast : Object, Mappable {
    @objc dynamic var cityID = 0
    @objc dynamic var cod = ""
    @objc dynamic var message = 0.0
    @objc dynamic var cnt = 0
    var list = List<ForecastListItem>()
    @objc dynamic var city: ForecastCity?
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "cityID"
    }
    
    
    func mapping(map: Map) {
        cod      <- map["cod"]
        message  <- map["message"]
        cnt      <- map["cnt"]
        list     <- (map["list"], ListTransform<ForecastListItem>())
        city        <- map["city"]
        
        if let c = city {
            cityID = c.id
        }
    }
}

