//
//  DetailedViewController.swift
//  Neme WEATHER
//
//  Created by Georgi Nikolaev on 19.07.18 г..
//  Copyright © 2018 г. Georgi Nikolaev. All rights reserved.
//
import Foundation
import UIKit
import HEXColor
import Realm
import RealmSwift

class DetailedPagerViewController : UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource, RefreshListenerViewControllerProtocol
{
    let realm = try! Realm();
    var cityData = CityData()
    var forecast = Forecast()
    var longForecast = LongForecast()
 
    func refresh(completed: () -> Void) {
        NSLog("💛 refresh data")
        updateViewControllers()
        completed()
    }

    func updateViewControllers() {
        for vc in orderedViewControllers {
            switch vc {
            case let vc as CityDataViewController:
                vc.show(cityData: cityData)
                break;
            case let vc as ForecastViewController:
                vc.show(forecast: forecast)
                break;
            case let vc as LongForecastViewController:
                vc.show(longForecast: longForecast)
                break;
            default:
                break;
            }
        }
    }
    
    func loadCurrentRealmData() {
        if let rCD = realm.object(ofType: CityData.self, forPrimaryKey: cityID) {
            cityData = rCD
        }

        if let rF = realm.object(ofType: Forecast.self, forPrimaryKey: cityID) {
            forecast = rF
        }

        if let rLF = realm.object(ofType: LongForecast.self, forPrimaryKey: cityID) {
            longForecast = rLF
        }
    }
    
    
    @IBOutlet weak var pageControl: UIPageControl!
    let gradientLayer = CAGradientLayer()
    var cityID: Int = 0 

    // MARK: UIPageViewControllerDataSource
    
    lazy var orderedViewControllers: [UIViewController] = {
        return [self.newViewController(viewController: "cityData"),
                self.newViewController(viewController: "cityForecast")]
    }()
    
    func newViewController(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Storyboard", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        self.delegate = self

        
        self.pageControl.frame = CGRect(origin: CGPoint(x: self.view.frame.width/2 - 24, y: self.view.frame.height - 90), size: self.pageControl.frame.size)
        self.pageControl.autoresizingMask = [.flexibleTopMargin, .flexibleWidth, .flexibleLeftMargin, .flexibleRightMargin]
        self.view.addSubview(self.pageControl)
        
        var nemetschekLogo = UIImageView(image: UIImage(named: "neme_logo"))
        nemetschekLogo.frame = CGRect(origin: CGPoint(x: self.view.frame.width/2 - 70, y: self.view.frame.height - 55), size: CGSize(width: 140, height: 31))
        nemetschekLogo.autoresizingMask = [.flexibleTopMargin, .flexibleWidth, .flexibleLeftMargin, .flexibleRightMargin]
        self.view.addSubview(nemetschekLogo)
        
        gradientLayer.frame = self.view.bounds
        let color1 = UIColor("#125685").cgColor as CGColor
        let color2 = UIColor("#236a9a").cgColor as CGColor
        let color3 = UIColor("#3677a5").cgColor as CGColor
        let color4 = UIColor("#1c5a8a").cgColor as CGColor
        let color5 = UIColor("#125686").cgColor as CGColor
        gradientLayer.colors = [color1, color2, color3, color4, color5]
        gradientLayer.locations = [0.0, 0.35, 0.60, 0.84, 1.0]
        self.view.layer.insertSublayer(gradientLayer, at: 0);
        
        for vc in self.orderedViewControllers {
            if var rVC = vc as? RefreshableViewController {
                rVC.refreshListener = self
            }
        }
        
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }

        self.view.bringSubview(toFront: self.pageControl)
        
        print("Show details for: \(cityID)")
        loadCurrentRealmData()
        updateViewControllers()
    }

    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.pageControl.currentPage = orderedViewControllers.index(of: pendingViewControllers.first!)!
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }

        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count

        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }

//    func presentationCount(for pageViewController: UIPageViewController) -> Int {
//        return orderedViewControllers.count
//    }
//
//    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
//        return 0
//    }

}
