import UIKit
import Alamofire
import Realm
import RealmSwift
import AlamofireObjectMapper

class MainViewController: UITableViewController, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    let realm = try! Realm();
    var isShowingFavorite = true
    var searchTerm = "*"
    var cityData:[CityData]
    
    required init?(coder aDecoder: NSCoder) {
        self.cityData = []
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getCityDataFromRealm(cityName city: String) -> CityData? {
        let city = realm.objects(CityData.self).filter { (cityData) -> Bool in
            let shouldIncludeInResults = (cityData.name.lowercased().range(of:city.lowercased()) != nil)
            return shouldIncludeInResults
            }.first
        return city
    }
    
    
    func getCityData(cityName city: String, result:@escaping (CityData)->Void) {
        let cityData = self.getCityDataFromRealm(cityName: city)
        if cityData != nil {
            result(cityData!)
            return
        }
        
        guard let url = URL(string: WeatherAPI.getCityDataURL(cityName: city)) else {
            return
        }
        
        Alamofire.request(url,
                          method: .get,
                          parameters: nil)
            .validate()
            .responseObject { (response: DataResponse<CityData>) in
                guard response.result.isSuccess else {
                    print("Error while trying to get city data for city with city \(city): \(String(describing: response.result.error))")
                    return
                }
                
                result(response.result.value!)
        }
    }
    
    func getCityForecast(cityID id: Int, result:(String)->Void) {
        guard let url = URL(string: WeatherAPI.getForecastURL(cityID: id)) else {
            return
        }
        
        print(url)

        Alamofire.request(url,
                          method: .get,
                          parameters: nil)
            .validate()
            .responseObject { (response: DataResponse<Forecast>) in
                guard response.result.isSuccess else {
                    print("Error while trying to get current forecast for city with id \(id): \(String(describing: response.result.error))")
                    return
                }
                
                print(response.result.value!)
        }
    }
    
    func getCityLongForecast(cityID id: Int, result:(String)->Void) {
        guard let url = URL(string: WeatherAPI.getLongForecastURL(cityID: id)) else {
            return
        }
        
        print(url)
        
        Alamofire.request(url,
                          method: .get,
                          parameters: nil)
            .validate()
            .responseObject { (response: DataResponse<LongForecast>) in
                guard response.result.isSuccess else {
                    print("Error while trying to get current long forecast for city with id \(id): \(String(describing: response.result.error))")
                    return
                }
                
                print(response.result.value!)
                
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let cities = realm.objects(CityData.self).filter { (cityData) -> Bool in
            var shouldIncludeInResults = cityData.favorite == self.isShowingFavorite
            if !self.isShowingFavorite
            {
                shouldIncludeInResults = shouldIncludeInResults && (cityData.name.lowercased().range(of:self.searchTerm.lowercased()) != nil)
            }
            return shouldIncludeInResults
        }
        
        if cities.count > 0 {
            self.cityData = cities.map{$0}
        } else {
            self.cityData = []
        }
        
        return self.cityData.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return searchTerm == "*" ? "Favorites" : "Result"
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 47
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 30))
        let label = UILabel(frame: CGRect(x: 15, y: 15, width: tableView.bounds.width - 30, height: 30))
//        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textColor = UIColor("#737378")
        label.text = searchTerm == "*" ? "Favorites" : "Result"
        headerView.addSubview(label)
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        
        cell.textLabel?.text = self.cityData[indexPath.row].name
        cell.tag = self.cityData[indexPath.row].id
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
//    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView
//    {
//        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
//        headerView.backgroundColor = UIColor(red:0.10, green:0.22, blue:0.32, alpha:1.0)
//
//        return headerView
//    }
//
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath)
//        if let cityData = self.getCityDataFromRealm(cityName: (cell?.textLabel!.text!)!) {
//            try? realm.write {
//                cityData.favorite = true//!cityData.favorite
//                print(cityData.favorite ? "favorite" : "unfavorite")
//            }
//        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count <= 2 {
            self.isShowingFavorite = true
            self.searchTerm = "*"
            print("Stop for: \(searchText)")
            self.tableView.reloadData()
            return;
        }

        print("Searching for: \(searchText)")
        if searchText.count > 2
        {
            self.isShowingFavorite = false
            self.searchTerm = searchText

            getCityData(cityName: searchText,result: { (cityData) in
                try? self.realm.write {
                    self.realm.add(cityData)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            });
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            guard let destVC = segue.destination as? DetailedPagerViewController else
            {
                return;
            }
            
            guard let cell = sender as? UITableViewCell else {
                return;
            }

            destVC.cityID = cell.tag
        }
    }

}
