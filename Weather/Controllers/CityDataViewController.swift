//
//  CityDataViewController.swift
//  Neme WEATHER
//
//  Created by Georgi Nikolaev on 30.07.18 г..
//  Copyright © 2018 г. Georgi Nikolaev. All rights reserved.
//

import Foundation
import UIKit

class CityDataViewController : UIViewController, RefreshableViewController
{
    var refreshControl: UIRefreshControl = UIRefreshControl()
    var refreshListener: RefreshListenerViewControllerProtocol?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lastUpdateLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    
    func show(cityData: CityData) {
        // Current temperature text update
        if let temperature = cityData.main?.temp
        {
            temperatureLabel.text = "\(Int(temperature))°C"
        }
        cityNameLabel.text = cityData.name;
        
        // Last update text update
        let date = Date(timeIntervalSince1970: Double(cityData.dt))
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        lastUpdateLabel.text = "Last updated: \(formatter.string(from: date))"
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        // If object has delegate execute refresh, otherwise do nothing
        
        guard let rl = refreshListener else {
            refreshControl.endRefreshing()
            return
        }

        rl.refresh {
            refreshControl.endRefreshing()
        };
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.white
        scrollView.isUserInteractionEnabled = true
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
//        refreshControl.frame = UIScreen.main.bounds
//        scrollView.
        self.scrollView.addSubview(refreshControl)
        self.scrollView.bringSubview(toFront: refreshControl)
    }
}
